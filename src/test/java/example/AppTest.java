package example;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import example.protobuf.Example;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.avro.Schema;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.protobuf.ProtobufData;
import org.apache.avro.protobuf.ProtobufDatumReader;
import org.apache.avro.protobuf.ProtobufDatumWriter;

/**
 * Unit test for simple App.
 */
public class AppTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testApp() throws IOException {
        final Example.User.Builder builder = Example.User.newBuilder();
        final Example.User user = builder.setFavoriteNumber(123).setName("Foo Barrington").build();

        final Schema schema = ProtobufData.get().getSchema(Example.User.class);

        final ProtobufDatumWriter<Example.User> writer = new ProtobufDatumWriter<Example.User>(schema);

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(out, null);
        writer.write(user, encoder);
        encoder.flush();

        final byte[] bytes = out.toByteArray();

        final ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        final BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(in, null);

        final ProtobufDatumReader<Example.User> reader = new ProtobufDatumReader<Example.User>(schema);
        final Example.User read = reader.read(Example.User.getDefaultInstance(), decoder);

    }
}
